function createCard(name, description, pictureUrl, start, end, locName) {
    return `
    <div class="card">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
            <h5 class="card-title">${name}</h5>
            <h6 class="card-subtitle">${locName}</h6>
            <p class="card-text">${description}</p>
            </div class="card-footer">
                <p class="card-text">${start}-${end}</p>
            </div>
        </div>
    </div>

    `;
}

function createPlaceholder(id) {
    return `
    <div class="card${id}" aria-hidden="true">
        <img src="..." class="card-img-top" alt="...">
        <div class="card-body">
            <h5 class="card-title placeholder-glow">
            <span class="placeholder col-6"></span>
            </h5>
            <p class="card-text placeholder-glow">
                <span class="placeholder col-7"></span>
                <span class="placeholder col-4"></span>
                <span class="placeholder col-4"></span>
                <span class="placeholder col-6"></span>
                <span class="placeholder col-8"></span>
            </p>
            <a href="#" tabindex="-1" class="btn btn-primary disabled placeholder col-6"></a>
        </div>
    </div>
    `
}

function createAlert() {
    return `
    <div class="alert alert-primary" role="alert">
        We could not find any conference!
    </div>
    `
}

window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
        const response = await fetch(url);

        if (!response.ok) {
        // Figure out what to do when the response is bad
        const row = document.querySelector('.row');
        const alertHtml = createAlert();
        row.innerHTML += alertHtml;

        } else {
        const data = await response.json();
        let i = 0


        for (let x = 0; x<data.conferences.length; x++) {
            const placeHtml = createPlaceholder(x+1);
            let column = document.querySelector(`.col${i%3 + 1}`);
            column.innerHTML += placeHtml;
            i++
        }

        i = 0
        for (let conference of data.conferences) {

            const detailUrl = `http://localhost:8000${conference.href}`;
            const detailResponse = await fetch(detailUrl);
            if (detailResponse.ok) {

                // let tempCard = document.querySelector(`.card${i+1}`)
                // tempCard.innerHTML = ""

                const details = await detailResponse.json();
                console.log(details)
                const title = details.conference.name;
                const description = details.conference.description;
                const pictureUrl = details.conference.location.picture_url;
                const start = new Date(details.conference.starts).toLocaleDateString();
                const end = new Date(details.conference.ends).toLocaleDateString();
                const locationName = details.conference.location.name

                const html = createCard(title, description, pictureUrl, start, end, locationName);
                // let column = document.querySelector(`.col${i%3 + 1}`);
                // column.innerHTML += html;
                let tempCard = document.querySelector(`.card${i+1}`)
                tempCard.innerHTML = html
                i++
            }
        }

        }
    } catch (e) {
      // Figure out what to do if an error is raised
        console.log('There was an error', e);
    }

});
