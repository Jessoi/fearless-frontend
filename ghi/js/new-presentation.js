window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/conferences/';
    const response = await fetch(url)
    if (response.ok) {
        const data = await response.json()
        console.log(data)

        const selectTag = document.getElementById('conference')

        for (let conference of data.conferences) {
            const option = document.createElement('option')
            option.value = conference.id
            option.innerHTML = conference.name
            selectTag.appendChild(option)
        }
    }

    const formTag = document.getElementById('create-presentation-form');

    formTag.addEventListener('submit', async event => {
        event.preventDefault();
        const formData = new FormData(formTag);
        const select = document.getElementById("conference")
        console.log(select.selectedIndex)
        const json = JSON.stringify(Object.fromEntries(formData));
        console.log(json)
        // const objData = Object.fromEntries(formData);
        // console.log(objData)
        // const presentationUrl = `http://localhost:8000/api/conferences/${objData.conference}/presentations/`;

        const presentationUrl = `http://localhost:8000/api/conferences/${select.selectedIndex}/presentations/`;

        console.log(presentationUrl)

        const fetchConfig = {
            method: "post",
            body: json,
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(presentationUrl, fetchConfig);
        if (response.ok) {
            formTag.reset();
            const newPresentation = await response.json();
            console.log(newPresentation);
        }
    });

});