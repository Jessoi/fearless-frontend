import React, { useEffect, useState } from 'react';

function PresentationForm() {
    const [conferences, setConferences] = useState([]);
    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [companyName, setCompanyName] = useState('');
    const [synopsis, setSynopsis] = useState('');
    const [presentationTitle, setPresentationTitle] = useState('');
    const [conference, setConference] = useState('');

    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }
    const handleEmailChange = (event) => {
        const value = event.target.value;
        setEmail(value);
    }
    const handleCompanyNameChange = (event) => {
        const value = event.target.value;
        setCompanyName(value);
    }
    const handlePresentationTitleChange = (event) => {
        const value = event.target.value;
        setPresentationTitle(value);
    }
    const handleSynopsisChange = (event) => {
        const value = event.target.value;
        setSynopsis(value);
    }
    const handleConferenceChange = (event) => {
        const value = event.target.value;
        setConference(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};

        data.presenter_name = name;
        data.presenter_email = email;
        data.company_name = companyName;
        data.title = presentationTitle;
        data.synopsis = synopsis;
        data.conference = conference;

        console.log(data);

        const locationUrl = `http://localhost:8000/api/conferences/${conference}/presentations/`;
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(locationUrl, fetchConfig);
        if (response.ok) {
            const newConference = await response.json();
            console.log(newConference);

            setName('');
            setEmail('');
            setPresentationTitle('');
            setCompanyName('');
            setSynopsis('');

        }
    }

    const fetchData = async () => {
        const url = `http://localhost:8000/api/conferences/`;
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setConferences(data.conferences);
            console.log(data)
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a new presentation</h1>
                    <form onSubmit={handleSubmit} id="create-presentation-form">
                        <div className="form-floating mb-3">
                            <input value={name} onChange={handleNameChange} placeholder="Name" required type="text" id="name" name="name" className="form-control" />
                            <label htmlFor="name">Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={email} onChange={handleEmailChange} required placeholder="Your email address" type="email" id="email" name="email" className="form-control" />
                            <label htmlFor="email">Your email address</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={companyName} onChange={handleCompanyNameChange} placeholder="Company Name" required type="text" name="companyName" id="companyName" className="form-control" />
                            <label htmlFor="company_name">Company Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={presentationTitle} onChange={handlePresentationTitleChange} placeholder="Presentation Title" required type="text" name="presentationTitle" id="presentationTitle" className="form-control" />
                            <label htmlFor="presentationTitle">Presentation Title</label>
                        </div>
                        <div className="form-floating mb-3">
                            <textarea value={synopsis} onChange={handleSynopsisChange} className="form-control" id="synopsis" name="synopsis" rows="5"></textarea>
                            <label htmlFor="synopsis">Synopsis</label>
                        </div>
                        <div className="mb-3">
                            <select onChange={handleConferenceChange} name="conference" id="conference" required>
                                <option value="">Choose a conference</option>
                                    {conferences.map(conference => {
                                        return (
                                            <option key={conference.id} value={conference.id}>{conference.name}</option>
                                        )
                                    })}
                            </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    )
}

export default PresentationForm;